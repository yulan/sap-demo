import React, {Component} from 'react';
import {DraggableAreasGroup} from '../Draggable';
import deleteBtn from '../imgs/delete.png';
import deleteBtn2x from '../imgs/delete@2x.png';
import { Button, Row, Col, Divider, Card, Avatar, } from 'antd';
import 'antd/dist/antd.css';
import styles from './style.less';
import mock from './mock.js';
<script type="text/javascript" src="js/other.js"></script>

const { Meta } = Card;

const group = new DraggableAreasGroup();
const DraggableArea1 = group.addArea();
const DraggableArea2 = group.addArea();
const DraggableArea3 = group.addArea();
const DraggableArea4 = group.addArea();
const DraggableArea5 = group.addArea();
const DraggableArea6 = group.addArea();
const DraggableArea7 = group.addArea();
const DraggableArea8 = group.addArea();
const DraggableArea9 = group.addArea(); 
function filterCd(min,max) {
  return mock.allTags.filter(word => word.score > min && word.score <= max)
}

export default class CrossArea extends Component {
  constructor() {
    super();
    this.state = {
      allTags: mock.allTags,
      oneTags: filterCd(60,70),
      twoTags: filterCd(70,80),
      threeTags: filterCd(80,100),
      fourTags: filterCd(30,40),
      fiveTags: filterCd(40,50),
      sixTags: filterCd(50,60),
      sevenTags: filterCd(0,10),
      eightTags: filterCd(10,20),
      nineTags: filterCd(20,30),
    }
  }

  filterCd(min,max){
    console.log(min,max);
    
    return this.state.allTags.filter(word => word.score > min && word.score <= max)
  }

  handleClickDelete(tag) {
    const rightTags = this.state.rightTags.filter(t => tag.id !== t.id);
    this.setState({rightTags});
  }

  math(l) {
    console.log('l', l);
    return (Math.round(l.length/this.state.allTags.length*100)+`%`)
  }

  render() {
    console.log('allTags', this.state.allTags);
    console.log('oneTags', this.state.oneTags);
    
    return (
      <div className="CrossArea">
        <Row className="row">
          <Col span={8}>
            <div>
              <div className="square oneTags">
                <div className="all">
                  <div className="left"></div>
                  <div className="right">{this.math(this.state.oneTags)}</div>
                </div>
                <DraggableArea1
                  className="panel"
                  tags={this.state.oneTags}
                  render={({tag}) => (
                    <Card>
                      {/* <Button onClick={this.onClick(tag)}> */}
                      <Meta
                        avatar={<Avatar src={tag.avatar} />}
                        title={tag.name}
                        description={tag.position}
                      />
                      {/* </Button> */}
                    </Card>
                  )}
                  onChange={oneTags => {
                    this.setState({oneTags});
                  }}
                />
              </div>
            </div>
          </Col>
          <Col span={8}>
            <div>
              <div className="square twoTags">
                <div className="all">
                  <div className="left"></div>
                  <div className="right">{this.math(this.state.twoTags)}</div>
                </div>
                <DraggableArea2
                  tags={this.state.twoTags}
                  render={({tag}) => (
                    <Card>
                      <Meta
                        avatar={<Avatar src={tag.avatar} />}
                        title={tag.name}
                        description={tag.position}
                      />
                    </Card>
                  )}
                  onChange={twoTags => {
                    this.setState({twoTags});
                  }}
                />
              </div>
            </div>
          </Col>
          <Col span={8}>
            <div>
              <div className="square threeTags">
                <div className="all">
                  <div className="left">核心领导者</div>
                  <div className="right">{this.math(this.state.threeTags)}</div>
                </div>
                <DraggableArea3
                  tags={this.state.threeTags}
                  render={({tag}) => (
                    <Card>
                      <Meta
                        avatar={<Avatar src={tag.avatar} />}
                        title={tag.name}
                        description={tag.position}
                      />
                    </Card>
                  )}
                  onChange={threeTags => {
                    this.setState({threeTags});
                  }}
                />
              </div>
            </div>
          </Col>
        </Row>
        <Row className="row">
          <Col span={8}>
            <div>
              <div className="square fourTags">
                <div className="all">
                  <div className="left"></div>
                  <div className="right">{this.math(this.state.fourTags)}</div>
                </div>
                <DraggableArea4
                  tags={this.state.fourTags}
                  render={({tag}) => (
                    <Card>
                    <Meta
                      avatar={<Avatar src={tag.avatar} />}
                      title={tag.name}
                      description={tag.position}
                    />
                  </Card>
                  )}
                  onChange={fourTags => {
                    this.setState({fourTags});
                  }}
                />
              </div>
            </div>
          </Col>
          <Col span={8}>
            <div>
              <div className="square fiveTags">
                <div className="all">
                  <div className="left">中坚员工</div>
                  <div className="right">{this.math(this.state.fiveTags)}</div>
                </div>
                <DraggableArea5
                  tags={this.state.fiveTags}
                  render={({tag}) => (
                    <Card>
                      <Meta
                        avatar={<Avatar src={tag.avatar} />}
                        title={tag.name}
                        description={tag.position}
                      />
                    </Card>
                  )}
                  onChange={fiveTags => {
                    this.setState({fiveTags});
                  }}
                />
              </div>
            </div>
          </Col>
          <Col span={8}>
            <div>
              <div className="square sixTags">
                <div className="all">
                  <div className="left">高影响力员工</div>
                  <div className="right">{this.math(this.state.sixTags)}</div>
                </div>
                <DraggableArea6
                  tags={this.state.sixTags}
                  render={({tag}) => (
                    <Card>
                      <Meta
                        avatar={<Avatar src={tag.avatar} />}
                        title={tag.name}
                        description={tag.position}
                      />
                    </Card>
                  )}
                  onChange={sixTags => {
                    this.setState({sixTags});
                  }}
                />
              </div>
            </div>
          </Col>
        </Row>
        <Row className="row">
          <Col span={8}>
            <div>
              <div className="square sevenTags">
                <div className="all">
                  <div className="left">有待提高</div>
                  <div className="right">{this.math(this.state.sevenTags)}</div>
                </div>
                <DraggableArea7
                  tags={this.state.sevenTags}
                  render={({tag}) => (
                    <Card>
                      <Meta
                        avatar={<Avatar src={tag.avatar} />}
                        title={tag.name}
                        description={tag.position}
                      />
                    </Card>
                  )}
                  onChange={sevenTags => {
                    this.setState({sevenTags});
                  }}
                />
              </div>
            </div>
          </Col>
          <Col span={8}>
            <div>
              <div className="square eightTags">
                <div className="all">
                  <div className="left">合格生产力员工</div>
                  <div className="right">{this.math(this.state.eightTags)}</div>
                </div>
                <DraggableArea8
                  tags={this.state.eightTags}
                  render={({tag}) => (
                    <Card>
                      <Meta
                        avatar={<Avatar src={tag.avatar} />}               
                        title={tag.name}
                        description={tag.position}
                      />
                    </Card>
                  )}
                  onChange={eightTags => {
                    this.setState({eightTags});
                  }}
                />
              </div>
            </div>
          </Col>
          <Col span={8}>
            <div>
              <div className="square nineTags">
                <div className="all">
                  <div className="left">高生产力员工</div>
                  <div className="right">{this.math(this.state.nineTags)}</div>
                </div>
                <DraggableArea9
                  tags={this.state.nineTags}
                  render={({tag}) => (
                    <Card>
                      <Meta
                        avatar={<Avatar src={tag.avatar} />}
                        title={tag.name}
                        description={tag.position}
                      />
                    </Card>
                  )}
                  onChange={nineTags => {
                    this.setState({nineTags});
                  }}
                />
              </div>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}