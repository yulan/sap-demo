export default {
  allTags:[{
      id: 1,
      avatar: '../imgs/1.png',
      name:'张三',
      position:'运营经理',
      store:'门店',
      score: 88
    },{
      id: 2,
      avatar: '../imgs/2.png',
      name:'李四',
      position:'销售总监',
      store:'门店',
      score: 80
    },{
      id: 3,
      avatar: '../imgs/3.png',
      name:'王二',
      position:'销售主管',
      store:'门店',
      score: 87
    },{
      id: 4,
      avatar: '../imgs/4.png',
      name:'李彦龙',
      position:'运营经理',
      store:'门店',
      score: 90
    },{
      id: 5,
      avatar: '../imgs/5.png',
      name:'李浩鹏',
      position:'销售总监',
      store:'门店',
      score: 88
    },{
      id: 6,
      avatar: '../imgs/3.png',
      name:'李雨泽',
      position:'销售主管',
      store:'门店',
      score: 83
    },{
      id: 7,
      avatar: '../imgs/1.png',
      name:'杨博瀚',
      position:'运营经理',
      store:'门店',
      score: 80
    },{
      id: 8,
      avatar: '../imgs/2.png',
      name:'杨涛了',
      position:'销售总监',
      store:'门店',
      score: 97
    },{
      id: 9,
      avatar: '../imgs/5.png',
      name:'杨思辰',
      position:'销售主管',
      store:'门店',
      score: 90
    },{
      id: 10,
      avatar: '../imgs/3.png',
      name:'罗童杰',
      position:'运营经理',
      store:'门店',
      score: 87
    },{
      id: 11,
      avatar: '../imgs/2.png',
      name:'罗赛互',
      position:'销售总监',
      store:'门店',
      score: 82
    },{
      id: 12,
      avatar: '../imgs/5.png',
      name:'罗金柱',
      position:'销售主管',
      store:'门店',
      score: 84
    },{
      id: 13,
      avatar: '../imgs/2.png',
      name:'杨博瀚',
      position:'运营经理',
      store:'门店',
      score: 86
    },{
      id: 14,
      avatar: '../imgs/1.png',
      name:'杨亚兰',
      position:'销售总监',
      store:'门店',
      cd: 5,
      score: 80
    },{
      id: 15,
      avatar: '../imgs/5.png',
      name:'杨子一',
      position:'销售主管',
      store:'门店',
      score: 90
    },{
      id: 16,
      avatar: '../imgs/1.png',
      name:'高骏臣',
      position:'运营经理',
      store:'门店',
      score: 91
    },{
      id: 17,
      avatar: '../imgs/4.png',
      name:'高凡力',
      position:'销售总监',
      store:'门店',
      score: 93
    },{
      id: 18,
      avatar: '../imgs/3.png',
      name:'高山洳',
      position:'销售主管',
      store:'门店',
      score: 95
    },{
      id: 19,
      avatar: '../imgs/5.png',
      name:'胡艺萱',
      position:'运营经理',
      store:'门店',
      score: 65
    },{
      id: 20,
      avatar: '../imgs/2.png',
      name:'胡雨萱',
      position:'销售总监',
      store:'门店',
      score: 67
    },{
      id: 21,
      avatar: '../imgs/5.png',
      name:'胡未迟',
      position:'销售主管',
      store:'门店',
      score: 68
    },{
      id: 22,
      avatar: '../imgs/2.png',
      name:'钟书豪',
      position:'运营经理',
      store:'门店',
      score: 69
    },{
      id: 23,
      avatar: '../imgs/1.png',
      name:'钟传庚',
      position:'销售总监',
      store:'门店',
      score: 73
    },{
      id: 24,
      avatar: '../imgs/4.png',
      name:'钟天浦',
      position:'销售主管',
      store:'门店',
      score: 72
    },{
      id: 25,
      avatar: '../imgs/1.png',
      name:'叶卓冉',
      position:'运营经理',
      store:'门店',
      score: 74
    },{
      id: 26,
      avatar: '../imgs/5.png',
      name:'叶思贝',
      position:'销售总监',
      store:'门店',
      score: 76
    },{
      id: 27,
      avatar: '../imgs/1.png',
      name:'叶青',
      position:'销售主管',
      store:'门店',
      score: 77
    },{
      id: 28,
      avatar: '../imgs/1.png',
      name:'张三',
      position:'运营经理',
      store:'门店',
      score: 63
    },{
      id: 29,
      avatar: '../imgs/2.png',
      name:'李四',
      position:'销售总监',
      store:'门店',
      score: 66
    },{
      id: 30,
      avatar: '../imgs/3.png',
      name:'王二',
      position:'销售主管',
      store:'门店',
      score: 64
    },{
      id: 31,
      avatar: '../imgs/4.png',
      name:'李彦龙',
      position:'运营经理',
      store:'门店',
      score: 62
    },{
      id: 32,
      avatar: '../imgs/5.png',
      name:'李浩鹏',
      position:'销售总监',
      store:'门店',
      score: 65
    },{
      id: 33,
      avatar: '../imgs/3.png',
      name:'李雨泽',
      position:'销售主管',
      store:'门店',
      score: 69
    },{
      id: 34,
      avatar: '../imgs/1.png',
      name:'杨博瀚',
      position:'运营经理',
      store:'门店',
      score: 77
    },{
      id: 35,
      avatar: '../imgs/2.png',
      name:'杨涛了',
      position:'销售总监',
      store:'门店',
      score: 71
    },{
      id: 36,
      avatar: '../imgs/5.png',
      name:'杨思辰',
      position:'销售主管',
      store:'门店',
      score: 73
    },{
      id: 37,
      avatar: '../imgs/3.png',
      name:'罗童杰',
      position:'运营经理',
      store:'门店',
      score: 73
    },{
      id: 38,
      avatar: '../imgs/2.png',
      name:'罗赛互',
      position:'销售总监',
      store:'门店',
      score: 75
    },{
      id: 39,
      avatar: '../imgs/5.png',
      name:'罗金柱',
      position:'销售主管',
      store:'门店',
      score: 74
    },{
      id: 40,
      avatar: '../imgs/2.png',
      name:'杨博瀚',
      position:'运营经理',
      store:'门店',
      score: 75
    },{
      id: 41,
      avatar: '../imgs/1.png',
      name:'杨亚兰',
      position:'销售总监',
      store:'门店',
      score: 78
    },{
      id: 42,
      avatar: '../imgs/5.png',
      name:'杨子一',
      position:'销售主管',
      store:'门店',
      score: 79
    },{
      id: 43,
      avatar: '../imgs/1.png',
      name:'高骏臣',
      position:'运营经理',
      store:'门店',
      score: 75
    },{
      id: 44,
      avatar: '../imgs/4.png',
      name:'高凡力',
      position:'销售总监',
      store:'门店',
      score: 75
    },{
      id: 45,
      avatar: '../imgs/3.png',
      name:'高山洳',
      position:'销售主管',
      store:'门店',
      score: 71
    },{
      id: 46,
      avatar: '../imgs/5.png',
      name:'胡艺萱',
      position:'运营经理',
      store:'门店',
      score: 75
    },{
      id: 47,
      avatar: '../imgs/2.png',
      name:'胡雨萱',
      position:'销售总监',
      store:'门店',
      score: 77
    },{
      id: 48,
      avatar: '../imgs/5.png',
      name:'胡未迟',
      position:'销售主管',
      store:'门店',
      score: 70
    },{
      id: 49,
      avatar: '../imgs/2.png',
      name:'钟书豪',
      position:'运营经理',
      store:'门店',
      score: 78
    },{
      id: 50,
      avatar: '../imgs/1.png',
      name:'钟传庚',
      position:'销售总监',
      store:'门店',
      score: 77
    },{
      id: 51,
      avatar: '../imgs/4.png',
      name:'钟天浦',
      position:'销售主管',
      store:'门店',
      score: 70
    },{
      id: 52,
      avatar: '../imgs/1.png',
      name:'叶卓冉',
      position:'运营经理',
      store:'门店',
      score: 71
    },{
      id: 53,
      avatar: '../imgs/5.png',
      name:'叶思贝',
      position:'销售总监',
      store:'门店',
      score: 72
    },{
      id: 54,
      avatar: '../imgs/1.png',
      name:'叶青',
      position:'销售主管',
      store:'门店',
      score: 77
    },{
      id: 55,
      avatar: '../imgs/1.png',
      name:'张三',
      position:'运营经理',
      store:'门店',
      score: 74
    },{
      id: 56,
      avatar: '../imgs/2.png',
      name:'李四',
      position:'销售总监',
      store:'门店',
      score: 68
    },{
      id: 57,
      avatar: '../imgs/3.png',
      name:'王二',
      position:'销售主管',
      store:'门店',
      score: 69
    },{
      id: 58,
      avatar: '../imgs/4.png',
      name:'李彦龙',
      position:'运营经理',
      store:'门店',
      score: 59
    },{
      id: 59,
      avatar: '../imgs/5.png',
      name:'李浩鹏',
      position:'销售总监',
      store:'门店',
      score: 65
    },{
      id: 60,
      avatar: '../imgs/3.png',
      name:'李雨泽',
      position:'销售主管',
      store:'门店',
      score: 44
    },{
      id: 61,
      avatar: '../imgs/1.png',
      name:'杨博瀚',
      position:'运营经理',
      store:'门店',
      score: 15
    },{
      id: 62,
      avatar: '../imgs/2.png',
      name:'杨涛了',
      position:'销售总监',
      store:'门店',
      score: 26
    },{
      id: 63,
      avatar: '../imgs/5.png',
      name:'杨思辰',
      position:'销售主管',
      store:'门店',
      score: 20
    },{
      id: 64,
      avatar: '../imgs/3.png',
      name:'罗童杰',
      position:'运营经理',
      store:'门店',
      score: 78
    },{
      id: 65,
      avatar: '../imgs/2.png',
      name:'罗赛互',
      position:'销售总监',
      store:'门店',
      score: 70
    },{
      id: 66,
      avatar: '../imgs/5.png',
      name:'罗金柱',
      position:'销售主管',
      store:'门店',
      score: 29
    },{
      id: 67,
      avatar: '../imgs/2.png',
      name:'杨博瀚',
      position:'运营经理',
      store:'门店',
      score: 39
    },{
      id: 68,
      avatar: '../imgs/1.png',
      name:'杨亚兰',
      position:'销售总监',
      store:'门店',
      score: 10
    },{
      id: 69,
      avatar: '../imgs/5.png',
      name:'杨子一',
      position:'销售主管',
      store:'门店',
      score: 66
    },{
      id: 70,
      avatar: '../imgs/1.png',
      name:'高骏臣',
      position:'运营经理',
      store:'门店',
      score: 68
    },{
      id: 71,
      avatar: '../imgs/4.png',
      name:'高凡力',
      position:'销售总监',
      store:'门店',
      score: 60
    },{
      id: 72,
      avatar: '../imgs/3.png',
      name:'高山洳',
      position:'销售主管',
      store:'门店',
      score: 54
    },{
      id: 73,
      avatar: '../imgs/5.png',
      name:'胡艺萱',
      position:'运营经理',
      store:'门店',
      score: 50
    },{
      id: 74,
      avatar: '../imgs/2.png',
      name:'胡雨萱',
      position:'销售总监',
      store:'门店',
      score: 68
    },{
      id: 75,
      avatar: '../imgs/5.png',
      name:'胡未迟',
      position:'销售主管',
      store:'门店',
      score: 43
    },{
      id: 76,
      avatar: '../imgs/2.png',
      name:'钟书豪',
      position:'运营经理',
      store:'门店',
      score: 82
    },{
      id: 77,
      avatar: '../imgs/1.png',
      name:'钟传庚',
      position:'销售总监',
      store:'门店',
      score: 35
    },{
      id: 78,
      avatar: '../imgs/4.png',
      name:'钟天浦',
      position:'销售主管',
      store:'门店',
      score: 85
    },{
      id: 79,
      avatar: '../imgs/1.png',
      name:'叶卓冉',
      position:'运营经理',
      store:'门店',
      score: 59
    },{
      id: 80,
      avatar: '../imgs/5.png',
      name:'叶思贝',
      position:'销售总监',
      store:'门店',
      score: 87
    }
  ],
}