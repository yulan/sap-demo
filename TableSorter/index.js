import React, {Component} from 'react';
import { Table  } from 'antd';
import 'antd/dist/antd.css';
import styles from './style.less';
import mock from '../CrossArea/mock.js';
<script type="text/javascript" src="js/other.js"></script>

export default class TableSorter extends Component {
  constructor() {
    super();
    this.state = {
      allTags: mock.allTags,
    }
  }

  onChange(pagination, filters, sorter, extra) {
    console.log('params', pagination, filters, sorter, extra);
  }

  render() {
    console.log('mock',mock.allTags);
    const columns = [
      {
        title: '姓名',
        dataIndex: 'name',
      },
      {
        title: '职位',
        dataIndex: 'position',
      },
      {
        title: '门店',
        dataIndex: 'store',
      },
      {
        title: '分数',
        dataIndex: 'score',
        sorter: {
          compare: (a, b) => a.score - b.score,
          multiple: 2,
        },
      }
    ];
    
    return (
      <div className="tableSorter">
        <Table 
          bordered
          key="id"
          columns={columns}
          dataSource={this.state.allTags}
          onChange={this.onChange} />
      </div>
    );
  }
}