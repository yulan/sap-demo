import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import CrossArea from './CrossArea';
import TableSorter from './TableSorter';
import { Tabs } from 'antd';
import styles from './style.less';
<script type="text/javascript" src="js/other.js"></script>

const { TabPane } = Tabs;
class Main extends Component {

  callback(key) {
    console.log(key);
  }

  render() {
    return (
      <div>
        <Tabs defaultActiveKey="1" onChange={this.callback}>
          <TabPane tab="列表视图" key="1">
            <TableSorter />
          </TabPane>
          <TabPane tab="绩效等级分布" key="2">
            <h3 className="section-title" id="CrossAreaDrag">
              绩效-潜能矩阵
            </h3>
            <CrossArea />
          </TabPane>
        </Tabs>
      </div>
    );
  }
}

const root = document.createElement('div');
document.body.appendChild(root);
ReactDOM.render(
  <Main />,
  root
);
